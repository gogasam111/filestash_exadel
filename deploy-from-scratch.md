# PART 1

the first thing we did we chose our project from the given list of projects from exadel which was the main part. the project had to be new. we chose filestash. Filestash lets you access and manage your data in a simple way regardless of where that data is stored. It can access data from google drive, gitlab, github mysql dropbox and many more.

<<<<<<< HEAD
# PART 2
=======
# Features

### Testing
- new setting
- Manage your files from a browser
- Full org mode client ([documentation](https://www.filestash.app/2018/05/31/release-note-v0.1/))
- Flexible Share mechanism
- Video player
- Video transcoding (mov, mkv, avi, mpeg, and more)
- Image viewer
- Image transcoding (raw images from Nikon, Canon, and more)
- Photo management
- Audio player
- Full Text Search
- Shared links are full fledge network drive
- Office documents (docx, xlsx and more)
- User friendly
- Mobile friendly
- Customisable
- Super fast
- Upload files and folders
- Multiple cloud providers and protocols, easily extensible
- Nyan cat loader
- Quick access: frequently access folders are pin to the homepage
- Emacs, VIM or Sublime keybindings `;)`
>>>>>>> b7185886dad723e5a53125006ac40382e701fdfe

Then we made a Gitlab reposirory we chose gitlab mainly for CI/CD we both agreed because we like Gitlab CI. the CI contains 6 jobs the first stage is code_quality which lints the code. if the code_quality is successfull we go on to next stage which is a before_build stage this stage creates repositorys for the build and copy-s config files there, it prepares the build. next we build frontend and backend. this was taken from the projects previous Drone CI where the maintainers of the project show us how the project is built. next we test the builds. since there are no test files in the project we test it manually by seeing if the files we built are present in the artifacts. and that includes our CI for the project

# PART 3

Next big thing to talk about is Kubernetes. we used GKE (google kubbernetes engine) for our project we used google cloud because of the credit google gives us which is 300$ for the registration which is a lot more then enough. we installed gcloud command line tool on the virtual machine and then we installed kubectl. The Kubernetes command-line tool, which allows us to run commands against Kubernetes clusters. then we generated and corrected the kubernetes manifest file. we generated the manifest automatically with kubectl create deployment, service, ingress etc. and dash minus O yaml at the end and we merged the file together in deployment.yaml file and gently made it work. 

# PART 4 

and we also integrated the kubernetes cluster to our gitlab repository. and added the monitoring for our kubernetes cluster in the gitlab. we installed prometheus on our cluster following the gitlab's how to add prometheus to kubernetes cluster guideline. which works great.

# PART 5. BACKUP
This app does not store any DB or external files. Therefore, we backed up only configuration files. For backup solution we used Rsync tool. Rsync is a fast and extraordinarily versatile file copying tool. It can copy locally, to/from another host over any remote shell, or to/from a remote rsync daemon. It offers a large number of options that control every aspect of its behavior and permit very flexible specification of the set of files to be copied. It is famous for its delta-transfer algorithm, which reduces the amount of data sent over the network by sending only the differences between the source files and the existing files in the destination. Rsync is widely used for backups and mirroring.
We created a EC2 instance on AWS for Backup job. In the backup server we added new shell command:

    rsync -azv remote_user@remote_host:/app/data/state /backups

Backup configured to run every 30 minutes using cron job.

For connecting two servers, we used ssh key pair.
